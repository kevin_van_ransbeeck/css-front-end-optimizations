# Example project to showcase some front-end optimizations.

Project based on the foundation framework/boilerplate.

## Requirements

You'll need to have the following items installed before continuing.

  * [Node.js](http://nodejs.org): Use the installer provided on the NodeJS website.
  * [Grunt](http://gruntjs.com/): Run `[sudo] npm install -g grunt-cli`
  * [Bower](http://bower.io): Run `[sudo] npm install -g bower`

## Quickstart

```bash
git clone git@git.qandisa.be:climacon/css_optim.git
cd css_optim
npm install && bower install
```

While you're working on your project, run:

`grunt build`
- Builds/combines/optimizes all assets.

`grunt critical`
- Builds/combines/optimizes all assets, compiles the index.php file into HTML and inlines the critical CSS.

`grunt`
- Runs the build task + watches your files for changes (SASS, images, JS...).

And you're set!

## Directory Structure

  * `scss/_settings.scss`: Foundation configuration settings go in here
  * `scss/_custom.scss`:   Application styles go here
