module.exports = function(grunt) {

  var mozjpeg = require('imagemin-mozjpeg');

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

/**
 * 1 :: Combine and uglify all JavaScript sources into 1 minified file.
 *      Start with (external) libraries that are really needed (dependencies).
 */
    uglify: {
      build: {
        files: {
          'js/app.min.js': ['js/libs/*.js', 'js/app.js']
        }
      }
    },

/**
 * 2 :: Process SASS (SCSS) files and compile their CSS version.
 */
    sass: {
      options: {
        // app.scss loads components from foundation.
        // Without this include, grunt would not know where to get them from.
        includePaths: ['bower_components/foundation/scss']
      },
      dist: {
        options: {
          // We will minify our final combined CSS file anyway.
          outputStyle: 'normal'
        },
        files: {
          'css/app.css': 'scss/app.scss',
        }
      }
    },

/**
 * 3 :: Combine different CSS files into 1 file.
 *      Start with (external) CSS (libs) that are really needed.
 */
    concat_css: {
      options: {
        // Task-specific options go here.
      },
      all: {
        src: ['css/**/*.css', '!css/style*css'],
        dest: 'css/style-unprefixed.css'
      },
    },

/**
 * 4 :: If there is CSS3 that needs browser-specific prefixes,
 *      this library will add these (create the different variants).
 *      On the contrary, if the original CSS contains prefixes that are no
 *      longer marked as needed, it will remove them (e.g. border-radius).
 */
    autoprefixer: {
      global: {
        files : {
          'css/style.css': 'css/style-unprefixed.css'
        }
      }
    },

/**
 * 5 :: Minify the prefixed CSS file to preserve bandwidth.
 */
    cssmin: {
      build: {
        src: 'css/style.css',
        dest: 'css/style.min.css'
      }
    },

/**
 * 6 :: Combine all svg files and create one mapped svg sprite we can include.
 */
    svgstore: {
      options: {
        prefix: "shape-",
        cleanup: false,
        svg: {
          style: "display: none;"
        }
      },
      default: {
        files: {
          'assets/svg-defs.svg': ['svg/*.svg']
        }
      }
    },

    /** @TODO: PNG fallback for IE */

/**
 * 7 :: Run PHP through parser to create static HTML
 *      (needed to include our critical css automatically).
 *      This is not possible on most dynamic sites.
 *      Search for alternative or extract critical CSS manually...
 */

    php2html: {
      default: {
        options: {
          // relative links should be renamed from .php to .html
          processLinks: true,
          htmlhint: false,
        },
        files: [
          {expand: false, cwd: './', src: ['*.php'], dest: './', ext: '.html' }
        ]
      }
    },

/**
 * 8 :: Extract critical CSS for a defined "above the fold" area.
 *      If more finetuning is desired: alternative with more options
 *      or external tool is needed.
 */

    penthouse: {
      extract : {
        outfile : './css/critical.css',
        css : './css/style.css',
        url : 'http://kevin.demo/cssoptim/index.html',
        width : 1024,
        height : 980
      },
    },

/**
 * 9 :: Automatically include CSS that is defined inline in the HTML file.
 *      Include the ?__inline=true query parameter on assets that need to be
 *      inlined. Example:
 *          <link href="css/critical.css?__inline=true" rel="stylesheet" />
 */
    inline: {
      dist: {
        options:{
          cssmin: true
        },
        src: ['index.html'],
      }
    },

/**
 * 10 :: Optimizes the images (png, jpg, gif) inside your assets/ folder.
 */
    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'assets/',
          src: ['**/*.{png,jpg,gif}'],
          dest: 'assets/'
        }]
      }
    },


/**
 * X :: Watch files for changes and act on them (recompile etc...).
 */
    watch: {
      options: {
        livereload: true,
      },

      grunt: {
        files: ['Gruntfile.js']
      },

      css: {
        files: ['scss/**/*.scss', 'css/libs/*.css'],
        tasks: ['sass', 'concat_css', 'autoprefixer', 'cssmin']
      },

      scripts: {
        files: ['js/**/*.js', '!js/**/*.min.js'],
        tasks: ['uglify']
      },

      svg: {
        files: 'svg/**/*.svg',
        tasks: ['svgstore']
      },

      images: {
        files: ['**/*.{png,jpg,gif}'],
        tasks: ['imagemin'],
      }
    }

  });

  // Load necessairy grunt tasks based on npm modules.
  require("load-grunt-tasks")(grunt);

  grunt.registerTask('critical', ['uglify', 'svgstore', 'sass', 'concat_css', 'autoprefixer', 'php2html', 'penthouse' ,'inline', 'cssmin', 'imagemin']);
  grunt.registerTask('build', ['uglify', 'svgstore', 'sass', 'concat_css', 'autoprefixer', 'cssmin', 'imagemin']);
  grunt.registerTask('default', ['build','watch']);
}
