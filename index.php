<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Demo site.</title>
    <link href="css/critical.css?__inline=true" rel="stylesheet" />


    <link rel="stylesheet" href="css/style.min.css" />
  </head>

  <body>

<?php
require_once ('assets/svg-defs.svg');
?>
<header>
    <div class="row">
      <div class="large-12 columns">
        <svg>
          <use xlink:href="#shape-kevinletterlogo" />
        </svg>
      </div>
      <div class="large-12 columns">
        <nav class="top-bar" data-topbar role="navigation">
          <ul class="title-area">
            <li class="name">
              <h1><a href="#">Demo Site</a></h1>
            </li>
             <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
          </ul>
          <section class="top-bar-section">
            <!-- Right Nav Section -->
            <ul class="right">
              <li class="active"><a href="index.php">Home</a></li>
              <li><a href="#">Blog</a></li>
              <li><a href="#">Portfolio</a></li>
              <li><a href="#">About</a></li>
              <li><a href="#">Contact</a></li>
            </ul>
          </section>
        </nav>
      </div>
    </div>
  </header>

    <div class="row">
      <div class="large-12 columns">
        <h1>Welcome to my Demo site.</h1>
      </div>
    </div>

    <div class="row">
      <div id="owl-demo" class="owl-carousel large-12 columns">
        <div class="item"><img class="lazyOwl" data-src="assets/slider/slider1.jpg" alt="Lazy Nature Image"></div>
        <div class="item"><img class="lazyOwl" data-src="assets/slider/slider2.jpg" alt="Lazy Nature Image"></div>
        <div class="item"><img class="lazyOwl" data-src="assets/slider/slider3.jpg" alt="Lazy Nature Image"></div>
        <div class="item"><img class="lazyOwl" data-src="assets/slider/slider4.jpg" alt="Lazy Nature Image"></div>
        <div class="item"><img class="lazyOwl" data-src="assets/slider/slider5.jpg" alt="Lazy Nature Image"></div>
        <div class="item"><img class="lazyOwl" data-src="assets/slider/slider6.jpg" alt="Lazy Nature Image"></div>
        <div class="item"><img class="lazyOwl" data-src="assets/slider/slider7.jpg" alt="Lazy Nature Image"></div>
        <div class="item"><img class="lazyOwl" data-src="assets/slider/slider8.jpg" alt="Lazy Nature Image"></div>
        <div class="item"><img class="lazyOwl" data-src="assets/slider/slider9.jpg" alt="Lazy Nature Image"></div>
        <div class="item"><img class="lazyOwl" data-src="assets/slider/slider10.jpg" alt="Lazy Nature Image"></div>
        <div class="item"><img class="lazyOwl" data-src="assets/slider/slider11.jpg" alt="Lazy Nature Image"></div>
      </div>
    </div>


    <div class="row">
      <div class="large-12 columns">
        <div class="panel">
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.</p>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="large-12 columns">
        <div class="countdown">
          <h3>Countdown to DrupalCamp Ghent <span class="strikethrough">2014</span> 2015</h3>
          <div id="clock"></div>
          <blockquote>
<p>Join us for a new Drupalcamp in Ghent on November 7 & 8, 2014. Our camp will be at the Campus Schoonmeersen of the Hogeschool Gent this year, walking distance from the Gent Sint-Pieters train station. We will be having multiple session tracks, codesprints and informal meetings (BoFs), which will add up to become the best Drupal event in Ghent ever!</p>
<p>Through a wide range of presentations you will learn more about all the latest in Drupal development and site building. The topics covered offer something for everyone, from novice to experienced user. They include talks about development, front end development, design & usability, community, and much more.</p>
          </blockquote>
          <div class="large-6 medium-6 columns push-3 drupalcamp">
            <a href="http://ghent2014.drupalcamp.be" target="_blank" class="cta-drupalcamp"><span><svg><use xlink:href="#shape-link-icon" /></svg></span>Visit the DrupalCamp site</a>
            <a href="https://www.facebook.com/DrupalcampBelgium" target="_blank" class="cta-facebook"><span><svg><use xlink:href="#shape-facebook-thumb" /></svg></span>Facebook page</a>
          </div>
        </div>
      </div>
    </div>

    <footer>
      <div class="row">
        <div class="large-4 medium-4 columns social">
          <a href="https://www.facebook.com/KevinVR" target="_blank">
            <svg class="facebook-icon">
              <use xlink:href="#shape-facebook-up" />
            </svg>
          </a>
          <a href="https://twitter.com/kevinvransbeeck" target="_blank">
            <svg class="twitter-icon">
              <use xlink:href="#shape-twitter-up" />
            </svg>
          </a>
          <a href="https://www.linkedin.com/profile/view?id=140537646" target="_blank">
            <svg class="linkedin-icon">
              <use xlink:href="#shape-linkedin-up" />
            </svg>
          </a>
          <a href="http://instagram.com/kevinvanransbeeck" target="_blank">
            <svg class="instagram-icon">
              <use xlink:href="#shape-instagram-up" />
            </svg>
          </a>
          <a href="http://www.pinterest.com/KevinVRansbeeck/" target="_blank">
            <svg class="pinterest-icon">
              <use xlink:href="#shape-pinterest-up" />
            </svg>
          </a>
        </div>
      </div>
    </footer>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/foundation/js/foundation.min.js"></script>

    <script src="js/app.min.js"></script>
  </body>
</html>
